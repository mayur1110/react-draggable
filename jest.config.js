const path = require("path");

module.exports = {
  moduleFileExtensions: ["js", "jsx"],
  setupFiles: [path.resolve(__dirname, "helpers/setupEnzyme.js")],
  testMatch: ["**/*.test.(js|jsx)"],
  modulePaths: ["src/"]
};
