import React from "react";
import PropTypes from "prop-types";

////////////////////

/**
 * class respresenting DraggableList component
 *
 * @class
 */
export default class DraggableList extends React.Component {
  static propTypes = {
    onOrderUpdate: PropTypes.func,
    children: PropTypes.array
  };

  /**
   * on drag start store a refrence of the target element.
   */
  dragStart = event => {
    this.dragged = event.currentTarget.closest(".item");
    event.dataTransfer.effectAllowed = "move";
    event.dataTransfer.setData("text/html", this.dragged);
  };

  /**
   * send from/to element index to consumer component.
   */
  dragEnd = () => {
    const from = Number(this.dragged.dataset.id);
    const to = Number(this.over.dataset.id);

    this.props.onOrderUpdate(from, to);
  };

  /**
   * store reference of the element where you want to drag the target element.
   */
  dragOver = event => {
    this.over = event.target.closest(".item");
  };

  /**
   * render component
   */
  render() {
    var listItems = this.props.children.map((item, i) => {
      return (
        <div
          className="item"
          data-id={i}
          key={i}
          draggable="true"
          onDragEnd={this.dragEnd}
          onDragStart={this.dragStart}
        >
          {item}
        </div>
      );
    });

    return (
      <div className="draggable-box" onDragOver={this.dragOver}>
        {listItems}
      </div>
    );
  }
}
