import DraggableList from "./draggableList";
import * as React from "react";
import { mount } from "enzyme";

describe("DraggableList", () => {
  const mockDraggableElement = <div />;
  const props = {
    onOrderUpdate: jest.fn(),
    children: [mockDraggableElement, mockDraggableElement, mockDraggableElement]
  };
  const mockDragArguments = {
    dataTransfer: {
      effectAllowed: "",
      setData: jest.fn()
    }
  };
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<DraggableList {...props} />);
  });

  test("should exist and have expected children elements", () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find("div.draggable-box")).toHaveLength(1);
    expect(wrapper.find("div.item")).toHaveLength(3);
  });

  test("should be able to drag an item from position 0 to 2", () => {
    const draggableItem = wrapper.find("div.item");
    draggableItem.at(0).simulate("dragStart", mockDragArguments);
    draggableItem.at(2).simulate("dragOver", mockDragArguments);
    draggableItem.at(0).simulate("dragend", {});

    expect(props.onOrderUpdate).toHaveBeenCalledWith(0, 2);
  });

  test("should be able to drag an item on its current position", () => {
    const draggableItem = wrapper.find("div.item");
    draggableItem.at(0).simulate("dragStart", mockDragArguments);
    draggableItem.at(0).simulate("dragOver", mockDragArguments);
    draggableItem.at(0).simulate("dragend", {});

    expect(props.onOrderUpdate).toHaveBeenCalledWith(0, 0);
  });
});
